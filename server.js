const jsonServer = require('json-server');
const bearerToken = require('express-bearer-token');
const multer = require('multer')
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

const upload = multer();

const OAUTH_TOKEN = '974b380a72bb6784dd4d2f4d4f9dd5c18d9c8174';
const VALID_CREDENTIALS = {
  email: 'pedron.albert@gmail.com',
  vat: '123456',
  token: '123456',
};

server.use(bearerToken());
server.use(middlewares);
server.use(jsonServer.bodyParser);


// Custom routes
server.post('/proposal_accept', (req, res) => {
  return res.json({ ok: 'Proposal accepted' }, 201);
});

server.post('/doc_list', upload.single('file'), (req, res) => {

  return res.json({
    id: req.body.id,
    doc_name: "DNI",
    status: "submitted",
    full_path: "http://foo.com/submitted_file.png",
    doc_type: 1,
   });
});

server.post('/token/email/auth', (req, res) => {
  const email = req.body.email;
  const vat = req.body.vat;
  const token = req.body.token;

  if (
    email === VALID_CREDENTIALS.email &&
    vat === VALID_CREDENTIALS.vat &&
    token === VALID_CREDENTIALS.token
  ) {
    return res.json({ token: OAUTH_TOKEN, refresh_limit: 60000 });
  }

  return res.sendStatus(401);
});

server.post('/token/refresh', (req, res) => {
  return res.json({ token: OAUTH_TOKEN, refresh_limit: 65000 });
});


// Auth check
const isAuthorized = ({ token }) => {
  return token === OAUTH_TOKEN;
};

server.use((req, res, next) => {
  const guardedRoutes = ['/bank_proposals', '/doc_list'];

  if (guardedRoutes.includes(req.path)) {
    if (isAuthorized(req)) {
      next();
    } else {
      return res.sendStatus(401);
    }
  }
});

// Throttle
server.use((req, res, next) => {
  setTimeout(() => {
    next();
  }, 500);
});

server.use(router);
server.listen(8081, () => {
  console.log('SERVER Running localhost:8081');
});

